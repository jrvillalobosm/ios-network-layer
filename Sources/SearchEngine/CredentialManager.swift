//
//  CredentialManager.swift
//
//  Created by Jorge Villalobos on 5/3/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

open class CredentialManager {
    public static let shared = CredentialManager()

    private var _password: String! = nil
    private var _username: String! = nil
    private var _baseURL: String! = nil

    public var password: String? {
        set {
            _password = newValue
        }
        get {
            if _password == nil {
                return SearchEngineConfigParams.password.getMessage()
            } else {
                return _password
            }
        }
    }

    public var username: String? {
        set {
            _username = newValue
        }
        get {
            if _username == nil {
                return SearchEngineConfigParams.username.getMessage()
            } else {
                return _username
            }
        }
    }

    public var baseURL: String? {
        set {
            _baseURL = newValue
        }
        get {
            if _baseURL == nil {
                return SearchEngineConfigParams.baseURL.getMessage()
            } else {
                return _baseURL
            }
        }
    }

    private init() { }
}
