//
//  SearchEngineRouter.swift
//
//  Created by Jorge Villalobos on 23/03/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Alamofire
import CommonsAssets
import Foundation
import HttpNetworkLayer
import SwiftyJSON

public enum SearchEngineDomain: ServiceDomain {
    case searchEngine

    public var baseURL: String? {
        return CredentialManager.shared.baseURL
    }

    public var credential: URLCredential? {
        guard CredentialManager.shared.username != nil && CredentialManager.shared.password != nil else {
            return nil
        }
        return URLCredential(user: CredentialManager.shared.username!,
            password: CredentialManager.shared.password!,
            persistence: .forSession)
    }
}

public enum SearchEngineRouter: URLRouter {
    case auth
    case searchByIndexType(String, String, Json)

    private var path: String {
        return SearchEngineConfigParams.path.getMessage()
    }

    private var index: String {
        switch self {
        case .searchByIndexType(let index, _, _):
            return index
        default:
            return ""
        }
    }

    public var serviceDomain: ServiceDomain {
        switch self {
        default:
            return SearchEngineDomain.searchEngine
        }
    }

    public var relativePath: String? {
        switch self {
        case .searchByIndexType(_, let type, _):
            return path.formatter(index, type)
        default:
            return nil
        }
    }

    public var method: HTTPMethod {
        switch self {
        case .searchByIndexType:
            return .post
        default:
            return .get
        }
    }

    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }

    public var params: Json? {
        switch self {
        case .searchByIndexType(_, _, let params):
            return params
        default:
            return nil
        }
    }
}
