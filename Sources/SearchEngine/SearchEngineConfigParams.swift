//
//  SearchEngineConfigParams.swift
//
//  Created by Jorge Villalobos on 20/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import DatabaseProxy
import Foundation

public extension RemoteConfigParams {
    static let searchEnginePath = RemoteConfigParam<String>("elastic_search_path")
    static let searchEngineUsername = RemoteConfigParam<String>("elastic_search_username")
    static let searchEnginePassword = RemoteConfigParam<String>("elastic_search_password")
    static let searchEngineUrlBase = RemoteConfigParam<String>("elastic_search_url_base")
    static let searchEngineIndexMessages = RemoteConfigParam<String>("elastic_search_index_messages")
    static let searchEngineIndexEntities = RemoteConfigParam<String>("elastic_search_index_entities")
    static let searchEngineIndexUser = RemoteConfigParam<String>("elastic_search_index_user")
    static let searchEngineIndex = RemoteConfigParam<String>("elastic_search_index")
}

public enum SearchEngineConfigParams: BundleMessage {
    case password
    case username
    case baseURL
    case entityIndex
    case messageIndex
    case userIndex
    case defaultIndex
    case path

    public var key: String {
        switch self {
        case .username:
            return RemoteConfigParams.searchEngineUsername._key
        case .password:
            return RemoteConfigParams.searchEnginePassword._key
        case .baseURL:
            return RemoteConfigParams.searchEngineUrlBase._key
        case .entityIndex:
            return RemoteConfigParams.searchEngineIndexEntities._key
        case .messageIndex:
            return RemoteConfigParams.searchEngineIndexMessages._key
        case .userIndex:
            return RemoteConfigParams.searchEngineIndexUser._key
        case .defaultIndex:
            return RemoteConfigParams.searchEngineIndex._key
        case .path:
            return RemoteConfigParams.searchEnginePath._key
        }
    }

    public func getMessage(_ args: [CVarArg] = []) -> String {
        let message: String
        switch RemoteConfigServices.shared.fetchStatus {
        case .success:
            switch self {
            case .username:
                message = RemoteConfigServices.shared[.searchEngineUsername]!
            case .password:
                message = RemoteConfigServices.shared[.searchEnginePassword]!
            case .baseURL:
                message = RemoteConfigServices.shared[.searchEngineUrlBase]!
            case .entityIndex:
                message = RemoteConfigServices.shared[.searchEngineIndexEntities]!
            case .messageIndex:
                message = RemoteConfigServices.shared[.searchEngineIndexMessages]!
            case .userIndex:
                message = RemoteConfigServices.shared[.searchEngineIndexUser]!
            case .defaultIndex:
                message = RemoteConfigServices.shared[.searchEngineIndex]!
            case .path:
                message = RemoteConfigServices.shared[.searchEnginePath]!
            }
            break
        default:
            message = BundleFacade.shared.displayMessage(bundleId: self.bundleId, key: self.key)
        }
        return message.formatter(args: args)
    }
}
