//
//  SearchEngineModel.swift
//
//  Created by Jorge Villalobos on 20/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation
import RxSwift
import SwiftyJSON

public class SearchEngineModel {
    private let json: JSON
    private let total: Int
    private let hits: [Json]
    private let aggregations: Json?

    public init(json: JSON) {
        self.json = json
        if let total = json["hits"]["total"].int {
            self.total = total
        } else {
            self.total = 0
        }
        if let array = json["hits"]["hits"].array {
            self.hits = array.map({ $0.dictionaryObject! })
        } else {
            self.hits = []
        }
        if let aggregations = json["aggregations"].dictionaryObject {
            self.aggregations = aggregations
        } else {
            self.aggregations = nil
        }
    }

    public func getTotal() -> Int {
        return self.total
    }

    public func getAggregations() -> Json? {
        return aggregations
    }

    public func getHits() -> [Json] {
        return self.hits
    }

    public func getHitsObservable() -> Observable<Any> {
        return Observable.create { observer in
            for object in self.hits {
                observer.onNext(object)
            }
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
