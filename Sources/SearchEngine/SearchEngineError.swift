//
//  SearchEngineError.swift
//
//  Created by Jorge Villalobos on 31/03/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation
import HttpNetworkLayer

public enum SearchEngineError: IError {

    case baseUrlNilValue
    case usernameNilValue
    case passwordNilValue
    case unknown(Error)

    public var bundleId: String {
        return HttpNetworkingConstants.error.description
    }

    public var code: Int {
        switch self {
        case .baseUrlNilValue:
            return 100
        case .usernameNilValue:
            return 200
        case .passwordNilValue:
            return 300
        default:
            return 0
        }
    }

    public var domain: String {
        switch self {
        default:
            return "SearchEngine"
        }
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case .unknown(let error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
