//
//  SearchEngine.swift
//
//  Created by Jorge Villalobos on 14/02/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation
import HttpNetworkLayer
import RxSwift
import SwiftyJSON

public enum SearchEngine {
    case shared

    static public func build() throws {
        //Probablemente agregar la validacion && Utils.isValidUrl(urlString: elastic.baseURL)
        guard CredentialManager.shared.baseURL != nil else {
            throw SearchEngineError.baseUrlNilValue
        }
        guard CredentialManager.shared.username != nil else {
            throw SearchEngineError.usernameNilValue
        }
        guard CredentialManager.shared.password != nil else {
            throw SearchEngineError.passwordNilValue
        }
        AlamofireProxy.request(SearchEngineRouter.auth).autentication(completion: { _ in })
    }

    static public func build(user: String, password: String, baseURL: String) throws {
        CredentialManager.shared.baseURL = baseURL
        CredentialManager.shared.password = password
        CredentialManager.shared.username = user
        try build()
    }

    public func getRequest(index: String, type: String, params: Json) -> Observable<SearchEngineModel> {
        return AlamofireProxy.request(SearchEngineRouter.searchByIndexType(index, type, params)).getResponse()
            .map() { json in
                return SearchEngineModel(json: json)
        }
    }

    @available(*, deprecated)
    public func getRequest(index: String,
        type: String,
        params: Json,
        completionHandler: @escaping (Response<SearchEngineModel>) -> ()
    ) {
        AlamofireProxy.request(SearchEngineRouter.searchByIndexType(index, type, params)).getResponse() { result in
            guard result.isSuccess else {
                completionHandler(Response.failure(result.error!))
                return
            }
            let json: JSON! = result.value
            completionHandler(Response.success(SearchEngineModel(json: json)))
        }
    }
}
