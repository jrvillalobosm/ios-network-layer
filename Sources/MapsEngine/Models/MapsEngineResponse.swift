//
//  MapsEngineResponse.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

open class MapsEngineResponse: Mappable {
    public var status: StatusCode?
    public var errorMessage: String?

    public init() { }
    public required init?(map: Map) { }

    public func mapping(map: Map) {
        status <- (map["status"], EnumTransform())
        errorMessage <- map["error_message"]
    }
}

public extension MapsEngineResponse {
    /// Reference: https://developers.google.com/maps/documentation/directions/intro#StatusCodes
    public enum StatusCode: String {
        case ok = "OK"
        case notFound = "NOT_FOUND"
        case zeroResults = "ZERO_RESULTS"
        case maxWaypointsExceeded = "MAX_WAYPOINTS_EXCEEDED"
        case invalidRequest = "INVALID_REQUEST"
        case overQueryLimit = "OVER_QUERY_LIMIT"
        case requestDenied = "REQUEST_DENIED"
        case unknownError = "UNKNOWN_ERROR"
    }

    /// Reference: https://developers.google.com/maps/documentation/directions/intro#RequestParameters
    public enum Place {
        case stringDescription(address: String)
        case coordinate(coordinate: LocationCoordinate2D)
        case placeID(id: String)

        public func toString() -> String {
            switch self {
            case .stringDescription(let address):
                return address
            case .coordinate(let coordinate):
                return "\(coordinate.latitude),\(coordinate.longitude)"
            case .placeID(let id):
                return "place_id:\(id)"
            }
        }
    }

    // GooglePlaces
    public enum PlaceType: String {
        case geocode = "geocode"
        case address = "address"
        case establishment = "establishment"
        case regions = "(regions)"
        case cities = "(cities)"
    }
}
