//
//  LocationCoordinate2D.swift
//
//  Created by Jorge Villalobos on 22/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

public typealias LocationDegrees = Double

public struct LocationCoordinate2D {
    public var latitude: LocationDegrees
    public var longitude: LocationDegrees

    public init(latitude: LocationDegrees, longitude: LocationDegrees) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

public class LocationCoordinate2DTransform: TransformType {
    public typealias Object = LocationCoordinate2D
    public typealias JSON = [String: Any]

    public func transformFromJSON(_ value: Any?) -> Object? {
        if let value = value as? JSON {
            guard let latitude = value["lat"] as? Double, let longitude = value["lng"] as? Double else {
                NSLog("Error: lat/lng is not Double")
                return nil
            }
            return LocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return nil
    }

    public func transformToJSON(_ value: Object?) -> JSON? {
        if let value = value {
            return [
                "lat": "\(value.latitude)",
                "lng": "\(value.longitude)"
            ]
        }
        return nil
    }
}
