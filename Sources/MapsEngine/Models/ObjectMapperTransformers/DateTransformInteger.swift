//
//  DateTransformInteger.swift
//
//  Created by Jorge Villalobos on 22/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

public class DateTransformInteger: TransformType {
    public typealias Object = Date
    public typealias JSON = Int

    public func transformFromJSON(_ value: Any?) -> Date? {
        if let timeInt = value as? Int {
            return Date(timeIntervalSince1970: TimeInterval(timeInt))
        }

        if let timeStr = value as? String {
            return Date(timeIntervalSince1970: TimeInterval(atof(timeStr)))
        }

        return nil
    }

    public func transformToJSON(_ value: Date?) -> Int? {
        if let date = value {
            return Int(date.timeIntervalSince1970)
        }
        return nil
    }
}
