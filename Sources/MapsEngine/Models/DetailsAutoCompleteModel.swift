//
//  DetailsAddressComponent.swift
//
//  Created by Jorge Villalobos on 22/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public struct DetailsAddressComponent {
    public var location: LocationCoordinate2D
    public var addressComponents: [PlaceDetailsResponse.Result.AddressComponent]
    public var locality: String = ""
    public var administrativeAreaLevel1: String = ""
    public var country: String = ""

    public init(location: LocationCoordinate2D, addressComponents: [PlaceDetailsResponse.Result.AddressComponent]) {
        self.location = location
        self.addressComponents = addressComponents
        for addressComponent in addressComponents {
            let types: [String] = addressComponent.types
            var isAccepted: Bool = true
            for type in types {
                switch type {
                case "locality":
                    self.locality = addressComponent.longName!
                    break
                case "administrative_area_level_1":
                    self.administrativeAreaLevel1 = addressComponent.longName!
                    break
                case "country":
                    self.country = addressComponent.longName!
                    break
                default:
                    isAccepted = false
                    break
                }
                if isAccepted == true {
                    break
                }
            }
        }
    }

    public func getFormattedAddress() -> String {
        var list: [String] = []
        if locality.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            list.append(locality)
        }
        if administrativeAreaLevel1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            list.append(administrativeAreaLevel1)
        }
        if country.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            list.append(country)
        }
        let formattedString = list.joined(separator: ", ")
        return formattedString
    }
}
