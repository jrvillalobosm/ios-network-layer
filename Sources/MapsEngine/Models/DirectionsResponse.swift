//
//  DirectionsResponse.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

public class DirectionsResponse: MapsEngineResponse {
    public var geocodedWaypoints: [Response.GeocodedWaypoint] = []
    public var routes: [Response.Route] = []

    public override init() {
        super.init()
    }
    public required init?(map: Map) {
        super.init(map: map)
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        geocodedWaypoints <- map["geocoded_waypoints"]
        routes <- map["routes"]
    }
}

public extension DirectionsResponse {
    public struct Response: Mappable {
        public var status: StatusCode?
        public var errorMessage: String?

        public var geocodedWaypoints: [GeocodedWaypoint] = []
        public var routes: [Route] = []

        public init() { }

        public init?(map: Map) { }

        public mutating func mapping(map: Map) {
            status <- (map["status"], EnumTransform())
            errorMessage <- map["error_message"]
            geocodedWaypoints <- map["geocoded_waypoints"]
            routes <- map["routes"]
        }

        public struct GeocodedWaypoint: Mappable {
            public var geocoderStatus: GeocoderStatus?
            public var partialMatch: Bool = false
            public var placeID: String?
            public var types: [AddressType] = []

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                geocoderStatus <- (map["geocoder_status"], EnumTransform())
                partialMatch <- map["geocoded_waypoints"]
                placeID <- map["place_id"]
                types <- (map["types"], EnumTransform())
            }
        }

        public struct Route: Mappable {
            public var summary: String?
            public var legs: [Leg] = []
            public var waypointOrder: [Int] = []
            public var overviewPolylinePoints: String?
            public var bounds: Bounds?
            public var copyrights: String?
            public var warnings: [String] = []
            public var fare: Fare?

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                summary <- map["summary"]
                legs <- map["legs"]
                waypointOrder <- map["waypointOrder"]
                overviewPolylinePoints <- map["overview_polyline.points"]
                bounds <- map["bounds"]
                copyrights <- map["copyrights"]
                warnings <- map["warnings"]
                fare <- map["fare"]
            }

            public struct Leg: Mappable {
                public var steps: [Step] = []
                public var distance: Step.Distance?
                public var duration: Step.Duration?
                public var durationInTraffic: DurationInTraffic?
                public var arrivalTime: Time?
                public var departureTime: Time?
                public var startLocation: LocationCoordinate2D?
                public var endLocation: LocationCoordinate2D?
                public var startAddress: String?
                public var endAddress: String?

                public init?(map: Map) { }

                public mutating func mapping(map: Map) {
                    steps <- map["steps"]
                    distance <- map["distance"]
                    duration <- map["duration"]
                    durationInTraffic <- map["duration_in_traffic"]
                    arrivalTime <- map["arrival_time"]
                    departureTime <- map["departure_time"]
                    startLocation <- (map["start_location"], LocationCoordinate2DTransform())
                    endLocation <- (map["end_location"], LocationCoordinate2DTransform())
                    startAddress <- map["start_address"]
                    endAddress <- map["end_address"]
                }

                public struct Step: Mappable {
                    /// formatted instructions for this step, presented as an HTML text string.
                    public var htmlInstructions: String?
                    public var distance: Distance?
                    public var duration: Duration?
                    public var startLocation: LocationCoordinate2D?
                    public var endLocation: LocationCoordinate2D?
                    public var polylinePoints: String?
                    public var steps: [Step] = []
                    public var travelMode: TravelMode?
                    public var maneuver: String?
                    public var transitDetails: TransitDetails?

                    public init?(map: Map) { }

                    public mutating func mapping(map: Map) {
                        htmlInstructions <- map["html_instructions"]
                        distance <- map["distance"]
                        duration <- map["duration"]
                        startLocation <- (map["start_location"], LocationCoordinate2DTransform())
                        endLocation <- (map["end_location"], LocationCoordinate2DTransform())
                        polylinePoints <- map["polyline.points"]
                        steps <- map["steps"]
                        travelMode <- map["travel_mode"]
                        maneuver <- map["maneuver"]
                        transitDetails <- map["transit_details"]
                    }

                    public struct Distance: Mappable {
                        public var value: Int? // the distance in meters
                        public var text: String? // human-readable representation of the distance

                        public init?(map: Map) { }

                        public mutating func mapping(map: Map) {
                            value <- map["value"]
                            text <- map["text"]
                        }
                    }

                    public struct Duration: Mappable {
                        public var value: Int? // the duration in seconds.
                        public var text: String? // human-readable representation of the duration.

                        public init?(map: Map) { }

                        public mutating func mapping(map: Map) {
                            value <- map["value"]
                            text <- map["text"]
                        }
                    }

                    public struct TransitDetails: Mappable {
                        public var arrivalStop: Stop?
                        public var departureStop: Stop?
                        public var arrivalTime: Time?
                        public var departureTime: Time?
                        public var headsign: String?
                        public var headway: Int?
                        public var numStops: Int?
                        public var line: TransitLine?

                        public init?(map: Map) { }

                        public mutating func mapping(map: Map) {
                            arrivalStop <- map["arrival_stop"]
                            departureStop <- map["departure_stop"]
                            arrivalTime <- map["arrival_time"]
                            departureTime <- map["departure_time"]
                            headsign <- map["headsign"]
                            headway <- map["headway"]
                            numStops <- map["num_stops"]
                            line <- map["line"]
                        }

                        public struct Stop: Mappable {
                            public var location: LocationCoordinate2D?
                            public var name: String?

                            public init?(map: Map) { }

                            public mutating func mapping(map: Map) {
                                location <- (map["location"], LocationCoordinate2DTransform())
                                name <- map["name"]
                            }
                        }

                        public struct TransitLine: Mappable {
                            public var name: String?
                            public var shortName: String?
                            public var color: UIColor?
                            public var agencies: [TransitAgency] = []
                            public var url: URL?
                            public var icon: URL?
                            public var textColor: UIColor?
                            public var vehicle: [TransitLineVehicle] = []

                            public init?(map: Map) { }

                            public mutating func mapping(map: Map) {
                                name <- map["name"]
                                shortName <- map["short_name"]
                                color <- (map["color"], colorTransform())
                                agencies <- map["agencies"]
                                url <- (map["url"], URLTransform())
                                icon <- (map["icon"], URLTransform())
                                textColor <- (map["text_color"], colorTransform())
                                vehicle <- map["vehicle"]
                            }

                            fileprivate func colorTransform() -> TransformOf<UIColor, String> {
                                return TransformOf<UIColor, String>(fromJSON: { (value: String?) -> UIColor? in
                                    if let value = value {
                                        return UIColor(hexString: value)
                                    }
                                    return nil
                                }, toJSON: { (value: UIColor?) -> String? in
                                        if let value = value {
                                            return value.hexString
                                        }
                                        return nil
                                    })
                            }

                            public struct TransitAgency: Mappable {
                                public var name: String?
                                public var phone: String?
                                public var url: URL?

                                public init?(map: Map) { }

                                public mutating func mapping(map: Map) {
                                    name <- map["name"]
                                    phone <- map["phone"]
                                    url <- (map["url"], URLTransform())
                                }
                            }

                            public struct TransitLineVehicle: Mappable {
                                public var name: String?
                                public var type: VehicleType?
                                public var icon: URL?

                                public init?(map: Map) { }

                                public mutating func mapping(map: Map) {
                                    name <- map["name"]
                                    type <- (map["type"], EnumTransform())
                                    icon <- (map["icon"], URLTransform())
                                }
                            }
                        }
                    }
                }

                public struct DurationInTraffic: Mappable {
                    public var value: Int? // the duration in seconds.
                    public var text: String? // human-readable representation of the duration.

                    public init?(map: Map) { }

                    public mutating func mapping(map: Map) {
                        value <- map["value"]
                        text <- map["text"]
                    }
                }

                public struct Time: Mappable {
                    public var value: Date? // the time specified as a JavaScript Date object.
                    public var text: String? // the time specified as a string.
                    // the time zone of this station. The value is the name of the time zone as defined in
                    // the IANA Time Zone Database, e.g. "America/New_York".
                    public var timeZone: TimeZone?

                    public init?(map: Map) { }

                    public mutating func mapping(map: Map) {
                        value <- (map["value"], DateTransformInteger())
                        text <- map["text"]
                        timeZone <- (map["time_zone"], timeZoneTransform())
                    }

                    fileprivate func timeZoneTransform() -> TransformOf<TimeZone, String> {
                        return TransformOf<TimeZone, String>(fromJSON: { (value: String?) -> TimeZone? in
                            if let value = value {
                                return TimeZone(identifier: value)
                            }
                            return nil
                        }, toJSON: { (value: TimeZone?) -> String? in
                                if let value = value {
                                    return value.identifier
                                }
                                return nil
                            })
                    }
                }
            }

            public struct Bounds: Mappable {
                public var northeast: LocationCoordinate2D?
                public var southwest: LocationCoordinate2D?

                public init?(map: Map) { }

                public mutating func mapping(map: Map) {
                    northeast <- (map["northeast"], LocationCoordinate2DTransform())
                    southwest <- (map["southwest"], LocationCoordinate2DTransform())
                }
            }

            public struct Fare: Mappable {
                public var currency: String?
                public var value: Float?
                public var text: String?

                public init?(map: Map) { }

                public mutating func mapping(map: Map) {
                    currency <- map["currency"]
                    value <- map["value"]
                    text <- map["text"]
                }
            }
        }
    }
}

public extension DirectionsResponse {

    public enum GeocoderStatus: String {
        case ok = "OK"
        case zeroResults = "ZERO_RESULTS"
    }

    public enum AddressType: String {
        case streetAddress = "street_address"
        case route = "route"
        case intersection = "intersection"
        case political = "political"
        case country = "country"
        case administrativeAreaLevel1 = "administrative_area_level_1"
        case administrativeAreaLevel2 = "administrative_area_level_2"
        case administrativeAreaLevel3 = "administrative_area_level_3"
        case administrativeAreaLevel4 = "administrative_area_level_4"
        case administrativeAreaLevel5 = "administrative_area_level_5"
        case colloquialArea = "colloquial_area"
        case locality = "locality"
        case ward = "ward"
        case sublocality = "sublocality"
        case sublocalityLevel1 = "sublocality_level_1"
        case sublocalityLevel2 = "sublocality_level_2"
        case sublocalityLevel3 = "sublocality_level_3"
        case sublocalityLevel4 = "sublocality_level_4"
        case sublocalityLevel5 = "sublocality_level_5"
        case neighborhood = "neighborhood"
        case premise = "premise"
        case subpremise = "subpremise"
        case postalCode = "postal_code"
        case naturalFeature = "natural_feature"
        case airport = "airport"
        case park = "park"
        case pointOfInterest = "point_of_interest"
    }

    public enum TravelMode: String {
        case driving = "DRIVING"
        case walking = "WALKING"
        case bicycling = "BICYCLING"
        case transit = "TRANSIT"
    }

    public enum RouteRestriction: String {
        case tolls = "tolls"
        case highways = "highways"
        case ferries = "ferries"
        case indoor = "indoor"
    }

    public enum TrafficMode: String {
        case bestGuess = "best_guess"
        case pessimistic = "pessimistic"
        case optimistic = "optimistic"
    }

    public enum TransitMode: String {
        case bus = "bus"
        case subway = "subway"
        case train = "train"
        case tram = "tram"
        case rail = "rail"
    }

    public enum TransitRoutingPreference: String {
        case lessWalking = "less_walking"
        case fewerTransfers = "fewer_transfers"
    }

    public enum UnitType: String {
        case metric = "metric"
        case imperial = "imperial"
    }

    public enum VehicleType: String {
        case rail = "RAIL"
        case metroRail = "METRO_RAIL"
        case subway = "SUBWAY"
        case tram = "TRAM"
        case monorail = "MONORAIL"
        case heavyRail = "HEAVY_RAIL"
        case commuterRail = "COMMUTER_TRAIN"
        case highSpeedTrain = "HIGH_SPEED_TRAIN"
        case bus = "BUS"
        case intercityBus = "INTERCITY_BUS"
        case trolleybus = "TROLLEYBUS"
        case shareTaxi = "SHARE_TAXI"
        case ferry = "FERRY"
        case cableCar = "CABLE_CAR"
        case gondolaLift = "GONDOLA_LIFT"
        case funicular = "FUNICULAR"
        case other = "OTHER"
    }
}
