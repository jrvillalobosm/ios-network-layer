//
//  PlaceDetailsResponse.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

public class PlaceDetailsResponse: MapsEngineResponse {
    public var result: Result?
    public var htmlAttributions: [String] = []

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init(map: map)
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
        htmlAttributions <- map["html_attributions"]
    }
}

public extension PlaceDetailsResponse {
    public struct Result: Mappable {
        public var addressComponents: [AddressComponent] = []
        public var formattedAddress: String?
        public var formattedPhoneNumber: String?
        public var geometryLocation: LocationCoordinate2D?
        public var icon: URL?
        public var internationalPhoneNumber: String?
        public var name: String?
        public var openingHours: OpeningHours?
        public var permanentlyClosed: Bool = false
        public var photos: [Photo] = []
        public var placeID: String?
        public var scope: Scope?
        public var alternativePlaceIDs: [PlaceIDScope] = []
        public var priceLevel: PriceLevel?
        public var rating: Float?
        public var reviews: [Review] = []
        public var types: [String] = []
        public var url: URL?
        public var utcOffset: Int?
        public var vicinity: String?
        public var website: String?

        public init() { }

        public init?(map: Map) { }

        public mutating func mapping(map: Map) {
            addressComponents <- map["address_components"]
            formattedAddress <- map["formatted_address"]
            formattedPhoneNumber <- map["formatted_phone_number"]
            geometryLocation <- (map["geometry.location"], LocationCoordinate2DTransform())
            icon <- (map["icon"], URLTransform())
            internationalPhoneNumber <- map["international_phone_number"]
            name <- map["name"]
            openingHours <- map["opening_hours"]
            permanentlyClosed <- map["permanently_closed"]
            photos <- map["photos"]
            placeID <- map["place_id"]
            scope <- (map["scope"], EnumTransform())
            alternativePlaceIDs <- map["alt_ids"]
            priceLevel <- map["price_level"]
            rating <- map["rating"]
            reviews <- map["reviews"]
            types <- map["types"]
            url <- map["url"]
            utcOffset <- map["utc_offset"]
            vicinity <- map["vicinity"]
            website <- map["website"]
        }

        public struct AddressComponent: Mappable {
            public var types: [String] = []
            public var longName: String?
            public var shortName: String?

            public init() { }

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                types <- map["types"]
                longName <- map["long_name"]
                shortName <- map["short_name"]
            }
        }

        public struct OpeningHours: Mappable {
            public var openNow: Bool = false
            public var periods: [Period] = []
            public var weekdayText: [String] = []

            public init() { }

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                openNow <- map["open_now"]
                periods <- map["periods"]
                weekdayText <- map["weekday_text"]
            }

            public struct Period: Mappable {
                public var open: DayTime?
                public var close: DayTime?

                public init() { }
                public init?(map: Map) { }

                public mutating func mapping(map: Map) {
                    open <- map["open"]
                    close <- map["close"]
                }

                public struct DayTime: Mappable {
                    public var day: Int?
                    public var time: Int?

                    public init() { }
                    public init?(map: Map) { }

                    public mutating func mapping(map: Map) {
                        day <- map["day"]
                        time <- map["time"]
                    }
                }
            }
        }

        public struct Photo: Mappable {
            public var photoReference: String?
            public var height: Float?
            public var width: Float?
            public var htmlAttributions: [String] = []

            public init() { }

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                photoReference <- map["photo_reference"]
                height <- map["height"]
                width <- map["width"]
                htmlAttributions <- map["html_attributions"]
            }
        }

        public enum Scope: String {
            case app = "APP"
            case google = "GOOGLE"
        }

        public struct PlaceIDScope: Mappable {
            public var placeID: String?
            public var scope: Scope?

            public init() { }

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                placeID <- map["place_id"]
                scope <- (map["scope"], EnumTransform())
            }
        }

        public enum PriceLevel: Int {
            case free = 0
            case inexpensive
            case moderate
            case expensive
            case veryExpensive
        }

        public struct Review: Mappable {
            public var aspects: [AspectRating] = []
            public var authorName: String?
            public var authorURL: URL?
            public var language: String?
            public var rating: Float?
            public var text: String?
            public var time: Date?
            public var reviewSummary: String?
            public var zagatSelected: Bool = false

            public init() { }

            public init?(map: Map) { }

            public mutating func mapping(map: Map) {
                aspects <- map["aspects"]
                authorName <- map["author_name"]
                authorURL <- (map["author_url"], URLTransform())
                language <- map["language"]
                rating <- map["rating"]
                text <- map["text"]
                time <- (map["time"], DateTransform())

                reviewSummary <- map["review_summary"]
                zagatSelected <- map["zagat_selected"]
            }

            public struct AspectRating: Mappable {
                public var type: Type?
                public var rating: Float?

                public init() { }

                public init?(map: Map) { }

                public mutating func mapping(map: Map) {
                    type <- map["type"]
                    rating <- map["rating"]
                }

                public enum `Type`: String {
                    case appeal = "appeal"
                    case atmosphere = "atmosphere"
                    case decor = "decor"
                    case facilities = "facilities"
                    case food = "food"
                    case overall = "overall"
                    case quality = "quality"
                    case service = "service"
                }
            }
        }
    }
}
