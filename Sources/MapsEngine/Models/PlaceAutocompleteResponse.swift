//
//  PlaceAutocompleteResponse.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import ObjectMapper

public class PlaceAutocompleteResponse: MapsEngineResponse {
    public var predictions: [Prediction] = []

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init(map: map)
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        predictions <- map["predictions"]
    }
}

public extension PlaceAutocompleteResponse {
    public struct Prediction: Mappable {
        public var description: String?
        public var place: Place?
        public var terms: [Term] = []
        public var types: [String] = []
        public var matchedSubstring: [MatchedSubstring] = []

        public init() { }

        public init?(map: Map) { }

        public mutating func mapping(map: Map) {
            description <- map["description"]
            place <- (map["place_id"], TransformOf(fromJSON: { (json) -> Place? in
                    if let placeId = json {
                        return Place.placeID(id: placeId)
                    } else {
                        return nil
                    }
                }, toJSON: { (place) -> String? in
                        switch place {
                        case .none:
                            return nil
                        case .some(let place):
                            switch place {
                            case .placeID(id: let id):
                                return id
                            default:
                                return nil
                            }
                        }
                    }))
            terms <- map["terms"]
            types <- map["types"]
            matchedSubstring <- map["matched_substrings"]
        }
    }

    public struct Term: Mappable {
        public var offset: Int?
        public var value: String?

        public init() { }

        public init?(map: Map) { }

        public mutating func mapping(map: Map) {
            offset <- map["offset"]
            value <- map["value"]
        }
    }

    public struct MatchedSubstring: Mappable {
        public var length: Int?
        public var offset: Int?

        public init() { }

        public init?(map: Map) { }

        public mutating func mapping(map: Map) {
            length <- map["length"]
            offset <- map["offset"]
        }
    }
}
