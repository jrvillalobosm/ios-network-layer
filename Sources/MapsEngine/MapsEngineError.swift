//
//  MapsEngineError.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import HttpNetworkLayer
import CommonsAssets
import Foundation

public enum MapsEngineError: IError {
	
	case apiKeyNotExisted
    case statusCodeNotFound
    case invalidRequest(String)
    case overQueryLimit(String)
	case unknown(String, String)
	
	public var bundleId: String {
		return HttpNetworkingConstants.error.description
	}
	
	public var code: Int {
		switch self {
		case .apiKeyNotExisted:
			return 100
        case .statusCodeNotFound:
            return 200
        case .invalidRequest:
            return 300
        case .overQueryLimit:
            return 400
		default:
			return 0
		}
	}
	
	public var domain: String {
		return "MapsEngine"
	}
	
    public func messageError(_ args: [CVarArg] = []) -> String {
		switch self {
		case .unknown(let message, let messageError):
			return "\(message) [\(messageError)]"
        case .invalidRequest(let messageError), .overQueryLimit(let messageError):
            let message : String = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key)
            return "\(message) [\(messageError)]"
		default:
			let message : String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key)
			return message
		}
	}
}
