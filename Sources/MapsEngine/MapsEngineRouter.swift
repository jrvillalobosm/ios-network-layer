//
//  MapsEngineRouter.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import HttpNetworkLayer
import CommonsAssets
import Foundation
import Alamofire

public enum MapsEngineRelativePaths: RelativePaths {
    
    case autocomplete
    case placeDetails
    case routeDirection
    
    public var description: String {
        switch self {
        case .autocomplete:
            return "place/autocomplete/json"
        case .placeDetails:
            return "place/details/json"
        case .routeDirection:
            return "directions/json"
        }
    }
}

public enum MapsEngineDomain: ServiceDomain {
    
    case googleMaps
    
    public var baseURL: String? {
        return "https://maps.googleapis.com/maps/api/"
    }
}

public enum MapsEngineRouter: URLRouter {
    
    case autocomplete(Json)
    case routeDirection(Json)
    case get(RelativePaths, Json)
    
    public var serviceDomain: ServiceDomain {
        switch self {
        default:
            return MapsEngineDomain.googleMaps
        }
    }
    
    public var relativePath: String? {
        switch self {
        case .get(let path, _):
            return path.description
        case .autocomplete:
            return MapsEngineRelativePaths.autocomplete.description
        case .routeDirection:
            return MapsEngineRelativePaths.routeDirection.description
        }
    }
    
    public var params: Json? {
        switch self {
        case .get(_, let params):
            return params
        case .autocomplete(let params):
            return params
        case .routeDirection(let params):
            return params
        }
    }
}
