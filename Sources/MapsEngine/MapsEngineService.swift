//
//  MapsEngineService.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import HttpNetworkLayer
import CommonsAssets
import Foundation
import Logger
import RxSwift

public class MapsEngineService: MapsEngineEndPoint {
    public static let shared = MapsEngineService()

    private let log: Logger = Logger.getLogger("MapsEngineService")

    private init() { }

    public func placeAutocomplete(forInput input: String,
        sessiontoken: String,
        offset: Int? = nil,
        locationCoordinate: LocationCoordinate2D? = nil,
        radius: Int? = nil,
        language: String? = nil,
        types: [MapsEngineResponse.PlaceType]? = nil,
        components: String? = nil,
        cancelPendingRequestsAutomatically: Bool = true
    ) -> Observable<PlaceAutocompleteResponse> {
        var requestParameters: [String: Any] = self.baseRequestParameters
            + ["input": input]
            + ["sessiontoken": sessiontoken]
        if let offset = offset {
            requestParameters["offset"] = offset
        }
        if let locationCoordinate = locationCoordinate {
            requestParameters["location"] = "\(locationCoordinate.latitude),\(locationCoordinate.longitude)"
        }
        if let radius = radius {
            requestParameters["radius"] = radius
        }
        if let language = language {
            requestParameters["language"] = language
        }
        if let types = types {
            requestParameters["types"] = types.map { $0.rawValue }.joined(separator: "|")
        }
        if let components = components {
            requestParameters["components"] = components
        }
        let observable: Observable<PlaceAutocompleteResponse> = AlamofireProxy
            .request(MapsEngineRouter.get(MapsEngineRelativePaths.autocomplete, requestParameters))
            .getMapperResponse()
        return observable.flatMap() { response -> Observable<PlaceAutocompleteResponse> in
            return self.complete(response: response)
        }
    }

    public func placeDetails(forPlaceID placeID: String,
        sessiontoken: String,
        extensions: String? = nil,
        language: String? = nil,
        fields: String? = "address_component,geometry"
    ) -> Observable<PlaceDetailsResponse> {
        var requestParameters = self.baseRequestParameters
            + ["placeid": placeID]
            + ["sessiontoken": sessiontoken]
        if let extensions = extensions {
            requestParameters["extensions"] = extensions
        }
        if let language = language {
            requestParameters["language"] = language
        }
        if let fields = fields {
            requestParameters["fields"] = fields
        }
        let observable: Observable<PlaceDetailsResponse> = AlamofireProxy
            .request(MapsEngineRouter.get(MapsEngineRelativePaths.placeDetails, requestParameters))
            .getMapperResponse()
        return observable.flatMap() { response -> Observable<PlaceDetailsResponse> in
            return self.complete(response: response)
        }
    }

    public func direction(fromOrigin origin: MapsEngineResponse.Place,
        toDestination destination: MapsEngineResponse.Place,
        travelMode: DirectionsResponse.TravelMode = .driving,
        wayPoints: [MapsEngineResponse.Place]? = nil,
        alternatives: Bool? = nil,
        avoid: [DirectionsResponse.RouteRestriction]? = nil,
        language: String? = nil, units: DirectionsResponse.UnitType? = nil,
        region: String? = nil,
        arrivalTime: Date? = nil,
        departureTime: Date? = nil,
        trafficModel: DirectionsResponse.TrafficMode? = nil,
        transitMode: DirectionsResponse.TransitMode? = nil,
        transitRoutingPreference: DirectionsResponse.TransitRoutingPreference? = nil
    ) -> Observable<DirectionsResponse> {
        var requestParameters: [String: Any] = self.baseRequestParameters + [
            "origin": origin.toString(),
            "destination": destination.toString(),
            "mode": travelMode.rawValue.lowercased()
        ]
        if let wayPoints = wayPoints {
            requestParameters["waypoints"] = wayPoints.map { $0.toString() }.joined(separator: "|")
        }
        if let alternatives = alternatives {
            requestParameters["alternatives"] = String(alternatives)
        }
        if let avoid = avoid {
            requestParameters["avoid"] = avoid.map { $0.rawValue }.joined(separator: "|")
        }
        if let language = language {
            requestParameters["language"] = language
        }
        if let units = units {
            requestParameters["units"] = units.rawValue
        }
        if let region = region {
            requestParameters["region"] = region
        }
        if arrivalTime != nil && departureTime != nil {
            self.log.warn("You can only specify one of arrivalTime or departureTime at most, requests may failed")
        }
        if let arrivalTime = arrivalTime, (travelMode == .transit || travelMode == .driving) {
            requestParameters["arrival_time"] = Int(arrivalTime.timeIntervalSince1970)
        }
        if let departureTime = departureTime, (travelMode == .transit || travelMode == .driving) {
            requestParameters["departure_time"] = Int(departureTime.timeIntervalSince1970)
        }
        if let trafficModel = trafficModel {
            requestParameters["traffic_model"] = trafficModel.rawValue
        }
        if let transitMode = transitMode {
            requestParameters["transit_mode"] = transitMode.rawValue
        }
        if let transitRoutingPreference = transitRoutingPreference {
            requestParameters["transit_routing_preference"] = transitRoutingPreference.rawValue
        }
        let observable: Observable<DirectionsResponse> = AlamofireProxy
            .request(MapsEngineRouter.get(MapsEngineRelativePaths.routeDirection, requestParameters))
            .getMapperResponse()
        return observable.flatMap() { response -> Observable<DirectionsResponse> in
            return self.complete(response: response)
        }
    }
}

public extension MapsEngineService {

    public func geocodeAddress(by text: String) -> Observable<LocationCoordinate2D> {
        return Location.geocodeAddressString(text)
            .filter() { placemark in
                guard (placemark.location?.coordinate) != nil else {
                    return false
                }
                return true
            }.map() { placemark -> LocationCoordinate2D in
                let coordinate = placemark.location!.coordinate
                return LocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }

    public func getDetailedAutocomplete(forInput input: String,
        sessiontoken: String,
        fields: String,
        offset: Int? = nil,
        locationCoordinate: LocationCoordinate2D? = nil,
        radius: Int? = nil,
        language: String? = nil,
        types: [MapsEngineResponse.PlaceType]? = nil,
        components: String? = nil,
        cancelPendingRequestsAutomatically: Bool = true
    ) -> Observable<DetailsAddressComponent?> {
        return MapsEngineService.shared.placeAutocomplete(forInput: input,
            sessiontoken: sessiontoken,
            offset: offset,
            locationCoordinate: locationCoordinate,
            radius: radius,
            language: language,
            types: types,
            components: components,
            cancelPendingRequestsAutomatically: cancelPendingRequestsAutomatically)
            .map() { value -> [PlaceAutocompleteResponse.Prediction]? in
                return value.predictions
            }.flatMap() { predictions -> Observable<PlaceAutocompleteResponse.Prediction> in
                return Observable.from(predictions!)
            }.flatMap() { prediction -> Observable<DetailsAddressComponent?> in
                return self.getFederatedAddress(place: prediction.place!, sessiontoken: sessiontoken, fields: fields, language: language)
                    .catchError { _ in Observable.empty() }
        }
    }

    public func getRouteDirection(fromOrigin origin: MapsEngineResponse.Place,
        toDestination destination: MapsEngineResponse.Place,
        travelMode: DirectionsResponse.TravelMode = .driving,
        wayPoints: [MapsEngineResponse.Place]? = nil,
        alternatives: Bool? = nil,
        avoid: [DirectionsResponse.RouteRestriction]? = nil,
        language: String? = nil,
        units: DirectionsResponse.UnitType? = nil,
        region: String? = nil,
        arrivalTime: Date? = nil,
        departureTime: Date? = nil,
        trafficModel: DirectionsResponse.TrafficMode? = nil,
        transitMode: DirectionsResponse.TransitMode? = nil,
        transitRoutingPreference: DirectionsResponse.TransitRoutingPreference? = nil
    ) -> Observable<String> {
        return self.direction(fromOrigin: origin,
            toDestination: destination,
            travelMode: travelMode,
            wayPoints: wayPoints,
            alternatives: alternatives,
            avoid: avoid,
            language: language,
            units: units,
            region: region,
            arrivalTime: arrivalTime,
            departureTime: departureTime,
            trafficModel: trafficModel,
            transitMode: transitMode,
            transitRoutingPreference: transitRoutingPreference)
            .flatMap() { value -> Observable<String> in
                let routes = value.routes
                return Observable.create { observer in
                    for route in routes {
                        observer.onNext(route.overviewPolylinePoints!)
                    }

                    observer.onCompleted()
                    return Disposables.create()
                }
        }
    }

    private func getFederatedAddress(place: MapsEngineResponse.Place,
        sessiontoken: String,
        fields: String,
        extensions: String? = nil,
        language: String? = nil
    ) -> Observable<DetailsAddressComponent?> {
        switch place {
        case .placeID(let placeId):
            return self.placeDetails(forPlaceID: placeId, sessiontoken: sessiontoken, extensions: extensions, language: language, fields: fields)
                .map() { details -> DetailsAddressComponent in
                    let result = details.result
                    return DetailsAddressComponent(location: (result?.geometryLocation!)!,
                        addressComponents: (result?.addressComponents)!)
            }
        default:
            return Observable.error(MapsEngineError.unknown("No PlaceId available", place.toString()))
        }
    }
}
