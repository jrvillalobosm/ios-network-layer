//
//  MapsEngineLocation.swift
//
//  Created by Jorge Villalobos on 3/30/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

public class Location {
    public class func geocodeAddressString(_ address: String) -> Observable<CLPlacemark> {
        let geocoder = CLGeocoder()
        return Observable.create { observer in
            geocoder.geocodeAddressString(address) { (placemarks, error) -> Void in
                guard let placemarks = placemarks else {
                    observer.onError(error!)
                    return
                }
                if placemarks.count > 0 {
                    observer.onNext(placemarks[0])
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }

    public class func reverseGeocodeLocation(_ location: CLLocation) -> Observable<CLPlacemark> {
        let geoCoder = CLGeocoder()
        return Observable.create { observer in
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in
                guard let placemarks = placemarks else {
                    observer.onError(error!)
                    return
                }
                if placemarks.count > 0 {
                    observer.onNext(placemarks[0])
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}
