//
//  MapsEngineEndPoint.swift
//
//  Created by Jorge Villalobos on 5/15/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import DatabaseProxy
import Foundation
import RxSwift

public extension RemoteConfigParams {
    static let googleAPIKey = RemoteConfigParam<String>("google_api_key")
}

public protocol MapsEngineEndPoint: BundleMessage {
    var baseRequestParameters: [String: String] { get }
}

public extension MapsEngineEndPoint {
    public var key: String {
        return RemoteConfigParams.googleAPIKey._key
    }

    public var baseRequestParameters: [String: String] {
        return ["key": self.getMessage()]
    }

    public func getMessage(_ args: [CVarArg] = []) -> String {
        let message: String
        switch RemoteConfigServices.shared.fetchStatus {
        case .success:
            message = RemoteConfigServices.shared[.googleAPIKey]!
            break
        default:
            message = BundleFacade.shared.displayMessage(bundleId: self.bundleId, key: self.key)
        }
        return message.formatter(args: args)
    }

    internal func complete<T : MapsEngineResponse>(response: T) -> Observable<T> {
        return Observable.create { observer in
            switch response.status {
            case .none:
                observer.onError(MapsEngineError.statusCodeNotFound)
            case .some(let status):
                switch status {
                case .ok:
                    observer.onNext(response)
                    observer.onCompleted()
                    break
                case .invalidRequest:
                    observer.onError(MapsEngineError.invalidRequest(response.errorMessage ?? ""))
                case .overQueryLimit:
                    observer.onError(MapsEngineError.overQueryLimit(response.errorMessage ?? ""))
                default:
                    observer.onError(MapsEngineError.unknown(status.rawValue, response.errorMessage ?? ""))
                }
            }
            return Disposables.create()
        }
    }
}
