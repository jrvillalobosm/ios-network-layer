//
//  AlamofireProxy.swift
//
//  Created by Jorge Villalobos on 5/12/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Alamofire
import CommonsAssets
import Foundation
import Logger
import ObjectMapper
import RxSwift
import SwiftyJSON

public enum AlamofireProxy: HttpNetworkingProtocol {
    case request(URLRouter)

    public var log: Logger {
        return Logger.getLogger("AlamofireProxy")
    }

    private var manager: Alamofire.SessionManager {
        return Alamofire.SessionManager.default
    }

    fileprivate var serviceDomain: ServiceDomain {
        switch self {
        case .request(let router):
            return router.serviceDomain
        }
    }

    fileprivate var requestType: URLRouter {
        switch self {
        case .request(let router):
            return router
        }
    }

    public func autentication(completion: @escaping (Response<JSON>) -> ()) {
        self.log.debug("Iniciar AUTENTICACION")
        guard serviceDomain.credential != nil else {
            completion(Response.failure(HttpNetworkingError.badCredentials))
            return
        }
        self.log.debug("URL ==> \(serviceDomain.baseURL!)")
        self.manager.request(serviceDomain.baseURL!)
            .authenticate(usingCredential: serviceDomain.credential!)
            .responseSwiftyJSON { response in
                self.handleResponse(response: response) { result in
                    if result.isSuccess {
                        self.log.debug("AUTENTICACION Successful")
                    } else {
                        self.log.debug("El error AUTENTICACION fue : \(result.error!.messageError())")
                        self.log.debug("CODIGO DE ERROR ===> \(response.response!.statusCode)")
                    }
                    completion(result)
                }
        }
    }

    public func getResponse(retryWithAuth: Bool = true, completion: @escaping (Response<JSON>) -> ()) {
        self.getDataRequest().responseSwiftyJSON { response in
            self.handleResponse(response: response) { result in
                if result.isFailure {
                    let error: IError! = result.error
                    if error.code == 401 && retryWithAuth {
                        self.autentication(completion: { result in
                            if result.isSuccess {
                                self.getResponse(retryWithAuth: false, completion: completion)
                            } else {
                                completion(result)
                            }
                        })
                        return
                    }
                }
                completion(result)
            }
        }
    }

    public func getResponse(retryWithAuth: Bool = true) -> Observable<JSON> {
        return Observable.create { observer in
            self.getResponse() { result in
                if result.isSuccess {
                    observer.onNext(result.value)
                    observer.onCompleted()
                } else {
                    observer.onError(result.error!)
                }
            }
            return Disposables.create()
        }
    }

    public func getMapperResponse<T: Mappable>(retryWithAuth: Bool = true) -> Observable<T> {
        return Observable.create { observer in
            self.getMapperResponse() { (response: Response<T>) in
                if response.isSuccess {
                    observer.onNext(response.value)
                    observer.onCompleted()
                } else {
                    observer.onError(response.error!)
                }
            }
            return Disposables.create()
        }
    }

    public func getMapperResponse<T: Mappable>(retryWithAuth: Bool = true, completion: @escaping (Response<T>) -> ()) {
        self.getResponseJSON() { response in
            guard response.isSuccess else {
                completion(Response.failure(response.error!))
                return
            }
            guard let json = response.value else {
                let error: IError = HttpNetworkingError.jsonSerializationFailed(HttpRequestError.parsingError.error)
                completion(Response.failure(error))
                return
            }
            guard let response = Mapper<T>().map(JSON: json) else {
                let error: IError = HttpNetworkingError.jsonSerializationFailed(HttpRequestError.mappingError.error)
                completion(Response.failure(error))
                return
            }

            completion(Response.success(response))
        }
    }

    public func getResponseJSON(retryWithAuth: Bool = true, completion: @escaping (Response<Json>) -> ()) {
        self.getDataRequest().responseJSON { response in
            self.handleResponse(response: response) { result in
                if result.isFailure {
                    let error: IError! = result.error
                    if error.code == 401 && retryWithAuth {
                        self.autentication(completion: { result in
                            if result.isSuccess {
                                self.getResponseJSON(retryWithAuth: false, completion: completion)
                            } else {
                                completion(Response.failure(result.error!))
                            }
                        })
                        return
                    }
                }
                if let _ = result.value as? NSNull {
                    completion(Response.success([:]))
                    return
                }
                guard let json = result.value as? Json else {
                    let error: IError = HttpNetworkingError.jsonSerializationFailed(HttpRequestError.parsingError.error)
                    completion(Response.failure(error))
                    return
                }
                completion(Response.success(json))
            }
        }
    }

    public func getDataRequest() -> DataRequest {
        let url = requestType.url
        let method = requestType.method
        let params = requestType.params
        let encoding = requestType.encoding
        let headers = requestType.headers
        return self.manager
            .request(url, method: method, parameters: params, encoding: encoding, headers: headers)
            .validate()
    }

    private func handleResponse<T>(response: DataResponse<T>?, completion: @escaping (Response<T>) -> ()) {
        let done: ((T?, IError?) -> ()) = { (data, error) in
            if let error = error {
                self.log.debug("ERROR ===> \(error)")
                switch error as! HttpNetworkingError {
                case .notResponse, .noInternetConnection:
                    completion(Response.failure(error))
                default:
                    guard response!.response != nil else {
                        completion(Response.failure(error))
                        return
                    }
                    self.log.debug("CODIGO DE ERROR ===> \(response!.response!.statusCode)")
                    if response!.response!.statusCode == 401 {
                        self.log.debug("ERROR - HAY  QUE AUTENTICAR!!!")
                        completion(Response.failure(HttpNetworkingError.unauthorized))
                    } else {
                        completion(Response.failure(error))
                    }
                }
            } else {
                completion(Response.success(data))
            }
        }

        if response == nil {
            done(nil, HttpNetworkingError.notResponse)
            return
        }

        switch response!.result {
        case .failure(let error):
            done(nil, error as? IError)
        case .success(let value):
            if response!.response!.statusCode != 200 {
                done(nil, HttpNetworkingError.requestDontFulfilled(response!.response!.statusCode))
            } else {
                done(value, nil)
            }
        }
    }
}
