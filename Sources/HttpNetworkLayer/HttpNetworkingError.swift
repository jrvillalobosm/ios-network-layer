//
//  HttpNetworkingError.swift
//
//  Created by Jorge Villalobos on 4/27/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public enum HttpNetworkingConstants: CustomStringConvertible {
    case configuration
    case error

    public var description: String {
        switch self {
        case .configuration:
            return "HttpNetworkingConfiguration"
        case .error:
            return "HttpNetworkingErrors"
        }
    }
}

public enum HttpRequestError {
    case parsingError
    case mappingError

    private var domain: String {
        return "HTTPRequestError"
    }

    private var code: Int {
        switch self {
        case .parsingError:
            return -2
        case .mappingError:
            return -3
        }
    }

    private var messageError: String {
        switch self {
        case .parsingError:
            return "Error: Parsing json failed"
        case .mappingError:
            return "Error: Mapping object response failed"
        }
    }

    public var error: NSError {
        return NSError(domain: self.domain, code: self.code, userInfo: [NSLocalizedDescriptionKey: self.messageError])
    }
}

public enum HttpNetworkingError: IError {
    case unauthorized
    case badCredentials
    case inputDataNilOrZeroLength
    case jsonSerializationFailed(Error)
    case notResponse
    case requestDontFulfilled(Int)
    case noInternetConnection
    case unknown(Error)

    public init(_ error: NSError) {
        let codeError: Int = error.code
        switch codeError {
        case -1009:
            self = .noInternetConnection
        default:
            self = .unknown(error)
        }
    }

    public var bundleId: String {
        return HttpNetworkingConstants.error.description
    }

    public var code: Int {
        switch self {
        case .inputDataNilOrZeroLength:
            return 100
        case .jsonSerializationFailed:
            return 200
        case .notResponse:
            return 300
        case .requestDontFulfilled:
            return 400
        case .unauthorized:
            return 401
        case .badCredentials:
            return 500
        case .noInternetConnection:
            return 600
        default:
            return 0
        }
    }

    public var domain: String {
        switch self {
        default:
            return "HttpNetworking"
        }
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case .requestDontFulfilled(let code):
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: [code])
            return message
        case .jsonSerializationFailed(let error):
            let message: String! = BundleFacade.shared
                .displayMessage(bundleId: bundleId, key: key, args: [error.localizedDescription])
            return message
        case .unknown(let error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
