//
//  URLRequestType.swift
//
//  Created by Jorge Villalobos on 4/28/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Alamofire
import CommonsAssets
import Foundation
import ObjectMapper
import RxSwift
import SwiftyJSON

public protocol RelativePaths: CustomStringConvertible { }

public protocol URLRequestType {
    var method: HTTPMethod { get }
    var params: Json? { get }
    var url: String { get }
    var headers: HTTPHeaders? { get }
    var encoding: ParameterEncoding { get }
}

public extension URLRequestType {
    var method: HTTPMethod {
        return .get
    }

    var headers: HTTPHeaders? {
        return nil
    }

    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
}

public protocol ServiceDomain {
    var baseURL: String? { get }
    var credential: URLCredential? { get }
}

public extension ServiceDomain {
    var credential: URLCredential? {
        return nil
    }
}

public protocol URLRouter: URLRequestType {
    var serviceDomain: ServiceDomain { get }
    var relativePath: String? { get }
}

public extension URLRouter {
    var url: String {
        var url = URL(string: self.serviceDomain.baseURL!)!
        if let relativePath = self.relativePath {
            url = url.appendingPathComponent(relativePath)
        }
        return url.absoluteString
    }
}

public protocol HttpNetworkingProtocol {
    func autentication(completion: @escaping (Response<JSON>) -> ())
    func getResponse(retryWithAuth: Bool, completion: @escaping (Response<JSON>) -> ())
    func getResponse(retryWithAuth: Bool) -> Observable<JSON>
    func getResponseJSON(retryWithAuth: Bool, completion: @escaping (Response<Json>) -> ())
    func getMapperResponse<T: Mappable>(retryWithAuth: Bool, completion: @escaping (Response<T>) -> ())
    func getMapperResponse<T: Mappable>(retryWithAuth: Bool) -> Observable<T>
}
