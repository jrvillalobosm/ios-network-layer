//
//  Alamofire+Extensions.swift
//
//  Created by Jorge Villalobos on 4/27/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

public extension Request {
    static func serializeResponseSwiftyJSON(
        options: JSONSerialization.ReadingOptions,
        response: HTTPURLResponse?,
        data: Data?,
        error: Error?)
        -> Result<JSON>
    {
        guard error == nil else { return .failure(HttpNetworkingError(error! as NSError)) }
        if let response = response, emptyDataStatusCodes.contains(response.statusCode) { return .success(JSON.null) }
        guard let validData = data, validData.count > 0 else {
            return .failure(HttpNetworkingError.inputDataNilOrZeroLength)
        }
        do {
            let json = try JSONSerialization.jsonObject(with: validData, options: options)
            return .success(JSON(json))
        } catch {
            return .failure(HttpNetworkingError.jsonSerializationFailed(error))
        }
    }
}

public extension DataRequest {
    static func swiftyJSONResponseSerializer(
        options: JSONSerialization.ReadingOptions = .allowFragments)
        -> DataResponseSerializer<JSON>
    {
        return DataResponseSerializer { _, response, data, error in
            return Request.serializeResponseSwiftyJSON(options: options, response: response, data: data, error: error)
        }
    }

    @discardableResult
    func responseSwiftyJSON(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler: @escaping (DataResponse<JSON>) -> Void)
        -> Self
    {
        return response(
            queue: queue,
            responseSerializer: DataRequest.swiftyJSONResponseSerializer(options: options),
            completionHandler: completionHandler
        )
    }
}

private let emptyDataStatusCodes: Set<Int> = [204, 205]
