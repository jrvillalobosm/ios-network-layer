//
//  Connectivity.swift
//
//  Created by Jorge Villalobos on 10/07/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import Alamofire
import CommonsAssets
import Foundation

public class Connectivity {
    public class func isConnectedToInternet() -> IError? {
        if (NetworkReachabilityManager()!.isReachable) {
            return nil
        } else {
            return HttpNetworkingError.noInternetConnection
        }
    }
}
